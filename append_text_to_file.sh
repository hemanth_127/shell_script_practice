#! /bin/bash

echo -e "enter name of file: \c "
read file_name

if [[ -f $file_name ]]; then
    if [ -w $file_name ]; then
        echo "Type some  text data ,press ctrl + d "
        cat >>$file_name
    else
        echo "you don't have permission to write in this file"
    fi
else
    echo "$file_name not found"
fi
