#! /bin/bash

# integer comparisons:
# -eq: Equal to
# -ne: Not equal to
# -lt: Less than
# -le: Less than or equal to
# -gt: Greater than
# -ge: Greater than or equal to

name=a

if [[ $name == a ]]; then
    echo "name is a"
else
    echo "name is not a"
fi

echo

# String comparision

# = or ==: Equal to
# !=: Not equal to
# <: Less than, in ASCII alphabetical order
# >: Greater than, in ASCII alphabetical order
# -z: String is null (has zero length)

#!/bin/bash

# Variables
str1="apple"
str2="banana"
str3=""

# Equal to
if (($str1 == $str2)); then
    echo "$str1 is equal to $str2"
else
    echo "$str1 is not equal to $str2"
fi

# Not equal to
if [[ $str1 != $str2 ]]; then
    echo "$str1 is not equal to $str2"
else
    echo "$str1 is equal to $str2"
fi

# Less than (ASCII alphabetical order)
if [[ $str1 < $str2 ]]; then
    echo "$str1 is less than $str2"
else
    echo "$str1 is not less than $str2"
fi

# Greater than (ASCII alphabetical order)
if [[ $str1 > $str2 ]]; then
    echo "$str1 is greater than $str2"
else
    echo "$str1 is not greater than $str2"
fi

len=${#str3}
if [[ $len -eq 0 ]]; then
    echo "$str1 length $len"
else
    echo "$len"
fi
