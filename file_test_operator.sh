#! /bin/bash

# -d dir
# -e exits
# -f file
# -s file empty or not


echo -e "enter name of file: \c "
read file_name

if [[ -f $file_name ]]; then
    echo "$file_name found"
else
    echo "$file_name not found"   
fi
