#!/bin/bash

# file reading
file="conditional.sh"
# while read p; do
#     echo $p
# done <$file

# 2nd way
# `cat $file | while read p; do
#     echo $p
# done`

# 3rd way

while IFS= read -r line; do
    echo $line
done <$file


