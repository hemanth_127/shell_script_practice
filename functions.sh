#! /bin/bash

# function print(){
#     # echo $1 $3 $2
#     name="$1 $2 $*"
#     echo $name
# }

# # quit(){
# #     exit 
# # }

# name="world in pa  in"

# print $name
# print king of hell



# ______________________________
# Function to check if a file exists

usage() {
    echo "You need to provide an argument :"
    echo "usage: $0 file_name"
}

is_file_exist() {
    local file="$1"  
    [[ -f "$file" ]] && return 0 || return 1 
}

[[ $# -eq 0 ]] && usage 

if (is_file_exist "$1"); then
    echo "$1 File found"  
else
    echo "File not found"  
fi

# ----------------------------

