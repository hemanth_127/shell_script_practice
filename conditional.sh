#!/bin/bash

# AND
# [ $age -gt 18 -a $age -lt 30 ];
# [ $age -gt 18 ] && [$age -lt 30 ];
# ____________________________________________

#  OR
# [ $age -gt 18 -o $age -lt 30 ];
# [ $age -gt 18 ] || [$age -lt 30 ];
# [[ $age -gt 18 -o $age -lt 30 ]];
age=25
if [ $age -gt 18 -a $age -lt 30 ]; then
    echo "You are eligible for voting"
else
    echo "You are not eligible for voting"
fi

# Example of logical NOT operator
# [ ! $age -gt 18 ];

age=15

if ! [ $age -ge 18 ]; then
    echo "You are not an adult"
else
    echo "You are an adult"
fi

day="Tuesday"
case $day in
"Monday")
    echo "Start of the work week."
    ;;
"Tuesday")
    echo "Second day of the work week."
    ;;
"Wednesday")
    echo "Midweek day."
    ;;
"Thursday")
    echo "Almost the weekend."
    ;;
"Friday")
    echo "Last work day of the week."
    ;;
"Saturday" | "Sunday")
    echo "It's the weekend!"
    ;;
*)
    echo "Invalid day."
    ;;
esac

# --------------------------------------------------------
# ex-2
echo -e "Enter some character\nread value\n"
read value

case $value in
[a-z])
    echo "User entered $value a to z"
    ;;
[A-Z])
    echo "User entered $value A to Z"
    ;;
[0-9])
    echo "User entered $value 0 to 9"
    ;;
?)
    echo "User entered $value special character"
    ;;
*)
    echo "Unknown input"
    ;;
esac
