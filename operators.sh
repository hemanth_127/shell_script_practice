#! /bin/bash

# Arithmetic operations
a=10
b=3

sum=$((a + b))        # Addition
difference=$((a - b)) # Subtraction
product=$((a * b))    # Multiplication
quotient=$((a / b))   # Division
remainder=$((a % b))  # Modulus

echo "Sum: $sum"
echo "Difference: $difference"
echo "Product: $product"
echo "Quotient: $quotient"
echo "Remainder: $remainder"

echo

# floating Numbers
# basic calculator <--bc
echo " scale=2;10.5 / 3" | bc -l
echo "10.5 + 3.0" | bc
echo "10 * 3" | bc 
echo "scale=2;10 % 3" | bc -l
echo "10.5 - 3" | bc -l
