#! /bin/bash

# while loop
# ----------------
# cnt=0
# while (( $cnt <= 3 )); do
#     echo $cnt
#     cnt=$((cnt + 1))
#     sleep 1
#     google-chrome & # it will open chrome
# done

# ______________________________________

# until loop
# ----------------

# cnt=0
# until (($cnt > 3)); do # it checks until the condition becomes true
#     echo $cnt
#     ((cnt++))
# done

# for loop
arr=(1 2 3 4 5)

# for i in ${arr[@]}; do
#     echo $i
# done

# for i in {1..10..2}; do
#     echo $i
# done

# for ((i = 0; i < 5; i++)); do
#     echo "${arr[$i]}"
# done

# /------------------------------------------
# -d dir checker
# -f file checker
# for item in *; do
#     if [ -d $item ]; then
#         echo $item
#     else
#         echo "not a directory"
#     fi

# done

# /------------------------------------------
# select loop

select name in mark john ben; do
    case $name in
    mark)
        echo $name
        break
        ;;
    john)
        echo $name
        break
        ;;
    ben)
        continue
        echo $name
        ;;
    *)
        echo "not found"
        ;;
    esac
done
