#! /bin/bash

trap "echo Exit signal is detected" SIGINT

echo "pid is $$"
while ((count < 10)); do
    sleep 5
    ((count++))
    echo "count is $count"
done
exit 0
